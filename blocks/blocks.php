<?php
// React Block Enqueue.
add_action( 'enqueue_block_editor_assets', 'bizland_block_assets' );

function bizland_block_assets() {
    wp_register_script(
        'bizland-gb-block',
        get_template_directory_uri(). '/blocks/js/blocks.build.js',
        [  'wp-blocks', 'wp-i18n', 'wp-element', 'wp-editor', 'wp-components' ]
    );

    // Editor css.
    wp_register_style(
        'bizland-gb-editor',
        get_template_directory_uri() . '/blocks/css/editor.css',
    );

    register_block_type(
        'bizland/blocks',
        [
            'editor_script' => 'bizland-gb-block',
            'editor_style' => 'bizland-gb-editor'
        ]
    );
}

// Frontend Style.
add_action( 'wp_enqueue_scripts', 'bizland_frontend_asset' );
function bizland_frontend_asset() {
    wp_enqueue_style( 'bizland-gb-frontend', get_template_directory_uri() . '/blocks/css/front.css' );
}