(function (wpI18n, wpBlocks, wpEditor, wpComponents, wpElement) {
	const { __ } = wpI18n;
	const { Fragment } = wpElement;
	const { registerBlockType } = wpBlocks;
	const { RichText, InspectorControls, InnerBlocks, MediaUpload, PanelColorSettings } = wpEditor;
	const { TextControl, PanelBody, PanelRow, RangeControl, SelectControl, ToggleControl, ColorPalette, IconButton, Button } = wpComponents;

	registerBlockType( 'bizland/header-banner', {
		title: __( 'Header Banner' ),
		// icon: '',
		category: 'common',
		keywords: [__('al alia block'), __('gutenberg'), __('Area2071')],
		attributes: {},
		edit: ( props ) => {
            return(
                <section id="hero" className="d-flex align-items-center">
                    <div className="container aos-init aos-animate" data-aos="zoom-out" data-aos-delay={100}>
                    <h1>Welcome to <span>BizLand</span></h1>
                    <h2>We are team of talented designers making websites with Bootstrap</h2>
                    <div className="d-flex">
                        <a href="#about" className="btn-get-started scrollto">Get Started</a>
                        <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" className="glightbox btn-watch-video"><i className="bi bi-play-circle" /><span>Watch Video</span></a>
                    </div>
                    </div>
                </section>
            )
        },
        save: ( props ) => {
            return(
                <section id="hero" className="d-flex align-items-center">
                    <div className="container aos-init aos-animate" data-aos="zoom-out" data-aos-delay={100}>
                    <h1>Welcome to <span>BizLand</span></h1>
                    <h2>We are team of talented designers making websites with Bootstrap</h2>
                    <div className="d-flex">
                        <a href="#about" className="btn-get-started scrollto">Get Started</a>
                        <a href="https://www.youtube.com/watch?v=jDDaplaOz7Q" className="glightbox btn-watch-video"><i className="bi bi-play-circle" /><span>Watch Video</span></a>
                    </div>
                    </div>
                </section>
            )
        }
	} );
})(wp.i18n, wp.blocks, wp.editor, wp.components, wp.element);