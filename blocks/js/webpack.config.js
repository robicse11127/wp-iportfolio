module.exports = {
    entry: './blocks.js',
    output: {
        path: __dirname,
        filename: 'blocks.build.js',
    },
    module: {
        rules: [
            {
                test: /.js$/,
                loader: 'babel-loader',
                exclude: /node_modules/,
            },
        ],
    },
};