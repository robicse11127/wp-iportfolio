<?php
/**
 * Bizland functions and definitions
 *
 * @link https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package bizland
 */

if ( ! function_exists( 'bizland_setup' ) ) :
	/**
	 * Sets up theme defaults and registers support for various WordPress features.
	 *
	 * Note that this function is hooked into the after_setup_theme hook, which
	 * runs before the init hook. The init hook is too late for some features, such
	 * as indicating support for post thumbnails.
	 */
	function bizland_setup() {
		/*
		 * Make theme available for translation.
		 * Translations can be filed in the /languages/ directory.
		 * If you're building a theme based on Crafty Press, use a find and replace
		 * to change 'bizland' to the name of your theme in all the template files.
		 */
		load_theme_textdomain( 'bizland', get_template_directory() . '/languages' );

		// Add default posts and comments RSS feed links to head.
		add_theme_support( 'automatic-feed-links' );

		/*
		 * Let WordPress manage the document title.
		 * By adding theme support, we declare that this theme does not use a
		 * hard-coded <title> tag in the document head, and expect WordPress to
		 * provide it for us.
		 */
		add_theme_support( 'title-tag' );

		/*
		 * Enable support for Post Thumbnails on posts and pages.
		 *
		 * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
		 */
		add_theme_support( 'post-thumbnails' );

		// This theme uses wp_nav_menu() in one location.
		register_nav_menus( array(
			'primary' => esc_html__( 'Primary', 'bizland' ),
		) );

		/**
		 * WooCommerce Support
		 */
		add_theme_support( 'woocommerce' );

		/*
		 * Switch default core markup for search form, comment form, and comments
		 * to output valid HTML5.
		 */
		add_theme_support( 'html5', array(
			'search-form',
			'comment-form',
			'comment-list',
			'gallery',
			'caption',
		) );

		// Set up the WordPress core custom background feature.
		add_theme_support( 'custom-background', apply_filters( 'bizland_custom_background_args', array(
			'default-color' => 'ffffff',
			'default-image' => '',
		) ) );

		// Add theme support for selective refresh for widgets.
		add_theme_support( 'customize-selective-refresh-widgets' );

		/**
		 * Add support for core custom logo.
		 */
		add_theme_support( 'custom-logo', array(
			'height'      => 250,
			'width'       => 250,
			'flex-width'  => true,
			'flex-height' => true,
		) );

		/**
		 * Add Support for Custom Page Header
		 */
		add_theme_support( 'custom-header', array(
			'flex-width'	=> true,
			'width'			=> 1600,
			'flex-height'	=> true,
			'height'		=> 450,
			'default-image' => get_template_directory_uri() .'/assets/dist/public/img/page-header.jpg',
		) );

		/**
		 * Add Editor Style
		 */
		add_editor_style( get_template_directory() . '/assets/dist/admin/css/editor-style.min.css' );
		
		/**
		 * Add Post Type Support
		 */
		// add_theme_support( 'post-formats', array( 'aside', 'gallery', 'link', 'image', 'quote', 'video', 'audio' ) );
	}
endif;
add_action( 'after_setup_theme', 'bizland_setup' );

/**
 * Enqueue scripts and styles.
 */
function bizland_public_scripts() {

	wp_enqueue_style( 'bizland-style', get_stylesheet_uri() );
	// wp_enqueue_style( 'bizland-google-fonts', bizland_google_fonts_url(), array(), null );
	wp_enqueue_style( 'bizland-bootstrap-style', get_template_directory_uri() . '/assets/css/main.css' );
	wp_enqueue_style( 'bizland-aos', get_template_directory_uri() . '/assets/vendor/aos/aos.css' );
	wp_enqueue_style( 'bizland-bootstrap', get_template_directory_uri() . '/assets/vendor/bootstrap/css/bootstrap.min.css' );
	wp_enqueue_style( 'bizland-bootstrap-icons', get_template_directory_uri() . '/assets/vendor/bootstrap-icons/bootstrap-icons.css' );
	wp_enqueue_style( 'bizland-boxicons', get_template_directory_uri() . '/assets/vendor/boxicons/css/boxicons.min.css' );
	wp_enqueue_style( 'bizland-glightbox', get_template_directory_uri() . '/assets/vendor/glightbox/css/glightbox.min.css' );
	wp_enqueue_style( 'bizland-swiper', get_template_directory_uri() . '/assets/vendor/swiper/swiper-bundle.min.css' );
	wp_enqueue_script( 'bizland-public-script', get_template_directory_uri() . '/assets/js/main.js', array( 'jquery' ), wp_rand(), true );
	if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
		wp_enqueue_script( 'comment-reply' );
	}
	// wp_add_inline_style( 'bizland-public-style', bizland_output_customizer_css() );
	// wp_add_inline_script( 'bizland-public-script', bizland_output_customizer_js() );
}
add_action( 'wp_enqueue_scripts', 'bizland_public_scripts' );

// Register Theme Menu.
register_nav_menus( [
    'primary' => __( 'Primary Menu', 'bizland' ),
    'top_menu' => __( 'Top Menu', 'bizland' ),
    'footer' => __( 'Footer Menu', 'bizland' ),
] );

// Require navwalker.
require_once get_template_directory() . '/navwalkers.php';
// Require Blocks.
require_once get_template_directory() . '/blocks/blocks.php';
