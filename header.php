<?php
/**
 * Header File.
 */
?><!DOCTYPE html>
<html <?php language_attributes(); ?>>

<head>
	<meta charset="<?php bloginfo( 'charset' ) ?>">
	<meta content="width=device-width, initial-scale=1.0" name="viewport">

	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Roboto:300,300i,400,400i,500,500i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

	<!-- =======================================================
	* Template Name: BizLand - v3.7.0
	* Template URL: https://bootstrapmade.com/bizland-bootstrap-business-template/
	* Author: BootstrapMade.com
	* License: https://bootstrapmade.com/license/
	======================================================== -->
	<?php wp_head(); ?>
</head>
<body <?php echo body_class(); ?>>

	<!-- ======= Top Bar ======= -->
	<section id="topbar" class="d-flex align-items-center">
		<div class="container d-flex justify-content-center justify-content-md-between">
		<div class="contact-info d-flex align-items-center">
			<i class="bi bi-envelope d-flex align-items-center"><a href="mailto:contact@example.com">contact@example.com</a></i>
			<i class="bi bi-phone d-flex align-items-center ms-4"><span>+1 5589 55488 55</span></i>
		</div>
		<div class="social-links d-none d-md-flex align-items-center">
			<a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
			<a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
			<a href="#" class="instagram"><i class="bi bi-instagram"></i></a>
			<a href="#" class="linkedin"><i class="bi bi-linkedin"></i></i></a>
		</div>
		</div>
	</section>

	<!-- ======= Header ======= -->
	<header id="header" class="d-flex align-items-center">
		<div class="container d-flex align-items-center justify-content-between">

			<h1 class="logo"><a href="<?php echo home_url(); ?>"><?php echo bloginfo( 'name' ); ?><span>.</span></a></h1>
			<!-- Uncomment below if you prefer to use an image logo -->
			<!-- <a href="index.html" class="logo"><img src="assets/img/logo.png" alt=""></a>-->

			<nav id="navbar" class="navbar">
				<?php
				if ( has_nav_menu( 'primary' ) ) :
					wp_nav_menu(
						[
							'theme_location' => 'primary',
							'container' => false,
							'menu_class' => '',
							'menu_id' => ''
						]
					);
				else :
					echo 'Assign menu';
				endif;
				?>
			</nav><!-- .navbar -->

		</div>
	</header><!-- End Header -->

  <?php
//   var_dump(get_template_directory_uri());